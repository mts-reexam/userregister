package com.register.userregister.util;

import com.register.userregister.dto.UserDTO;

import java.util.ArrayList;
import java.util.List;

public class MockUser {

    public static UserDTO mockUser(String name, String password, String email) {
        return new UserDTO(name, password.toCharArray(), email);
    }


    public static ArrayList<UserDTO> getUsersList(boolean correctNames, boolean correctPasswords, boolean correctEmails) {
        ArrayList<UserDTO> usersList = new ArrayList<>();
        ArrayList<String> namesList = correctNames ? getCorrectNames() : getIncorrectNames();
        ArrayList<String> passwordsList = correctPasswords? getCorrectPasswords() : getIncorrectPasswords();
        ArrayList<String> emailsList = correctEmails ? getCorrectEmails() : getIncorrectEmails();
        for (String name : namesList){
            for (String password : passwordsList){
                for (String email : emailsList){
                    usersList.add(mockUser(name,password,email));
                }
            }
        }
        return usersList;
    }






    public static ArrayList<String> getCorrectNames() {
        return new ArrayList<>(List.of(
                "tw",
                "twentyyyyyyyyyyyyyyy",
                "TW",
                "TWENTYYYYYYYYYYYYYYY",
                "user1",
                "user22",
                "user333",
                "user4444",
                "user55555",
                "BIGsmall",
                "smallBIG",
                "BiGaNdSmAlL",
                "BIG1",
                "2BIG",
                "3BIG3",
                "small1",
                "2small",
                "3small3",
                "1BIGsmall",
                "BIGsmall2",
                "3BIGsmall3"
        ));
    }


    public static ArrayList<String> getIncorrectNames() {
        return new ArrayList<>(List.of(
                "a",
                "B",
                "1",
                ",",
                "twentyoneeeeeeeeeeeee",
                "TWENTYONEEEEEEEEEEEEE",
                "123456789101112131415",
                "twentyoneeeeeeeeeeee1",
                "TWENTYONEEEEEEEEEEEE1",
                "2twentyoneeeeeeeeeeee",
                "2TWENTYONEEEEEEEEEEEE",
                "3twentyoneeeeeeeeeee3",
                "3TWENTYONEEEEEEEEEEE3"
        ));
    }

    public static ArrayList<String> getCorrectPasswords() {
        return new ArrayList<>(List.of(
                "fivee",
                "thirtyyyyyyyyyyyyyyyyyyyyyyyyy",
                "FIVEE",
                "THIRTYYYYYYYYYYYYYYYYYYYYYYYYY",
                "smallBIG",
                "BIGsmall",
                "BiGaNdSmAlL",
                "12345",
                "asfdsajghpasfdumh3701242"

        ));


    }

    public static ArrayList<String> getIncorrectPasswords() {
        return new ArrayList<>(List.of(
                "1",
                "22",
                "333",
                "4444",
                "a",
                "a2",
                "3a3",
                "4a4a",
                "thirtyoneeeeeeeeeeeeeeeeeeeeeee",
                "THIRTYONEEEEEEEEEEEEEEEEEEEEEEE",
                "12345678910111213141516171819202",
                "1thirtyoneeeeeeeeeeeeeeeeeeeeeee",
                "1THIRTYONEEEEEEEEEEEEEEEEEEEEEEE",
                "thirtyoneeeeeeeeeeeeeeeeeeeeeee2",
                "THIRTYONEEEEEEEEEEEEEEEEEEEEEEE2",
                "3hirtyoneeeeeeeeeeeeeeeeeeeeeee3",
                "3THIRTYONEEEEEEEEEEEEEEEEEEEEEE3"

        ));
    }
    public static ArrayList<String> getCorrectEmails() {
        return new ArrayList<>(List.of(
                "user@gmail.com",
                "123@123.com",
                "user@123.com",
                "123@gmail.com"
        ));
    }
    public static ArrayList<String> getIncorrectEmails() {
        return new ArrayList<>(List.of(
                "user@",
                "user",
                "user@.com",
                "user@@gmail.com",
                "user@gmail..com",
                "@gmail.com",
                "@@gmail.",
                "user@gmail",
                "usr.com@gmaiul"
        ));
    }
}
