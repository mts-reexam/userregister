package com.register.userregister;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("develop")
class UserRegisterApplicationTests {

    @Test
    void contextLoads() {
    }

}
