package com.register.userregister.controller;


import com.register.userregister.dto.UserDTO;
import com.register.userregister.exception.NotUniqueEmailException;
import com.register.userregister.response.NotUniqueEmailResponseBody;
import com.register.userregister.response.SuccessRegistrationResponseBody;
import com.register.userregister.service.RegisterUserServiceImpl;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static com.register.userregister.util.AsJsonString.asJsonString;
import static com.register.userregister.util.MockUser.getUsersList;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RegistrationController.class)
public class ControllerTest {

    @MockBean
    private RegisterUserServiceImpl service;

    @Autowired
    private MockMvc mockMvc;
    

    @Test
    void registerCorrectUser_Test() throws Exception {
        ArrayList<UserDTO> userDTOList = getUsersList(
                true,
                true,
                true);
        for (UserDTO userDTO : userDTOList) {
            Mockito.reset(service);
            registerCorrectUser_returnSuccessRegistrationResponse(userDTO);
        }
    }

    @Test
    void registerUserWithSameEmail_returnNotUniqueEmailResponse_Test() throws Exception {
        ArrayList<UserDTO> userDTOList = getUsersList(
                true,
                true,
                true);
        for (UserDTO userDTO : userDTOList) {
            Mockito.reset(service);
            registerUserWithSameEmail_returnNotUniqueEmailResponse(userDTO);
        }
    }

    @Test
    void registerUserWithIncorrectName_Test() throws Exception {
        ArrayList<UserDTO> userDTOList = getUsersList(
                false,
                true,
                true);
        for (UserDTO userDTO : userDTOList) {
            registerUserWithIncorrectName_returnValidationExceptionResponseBody(userDTO);
        }
    }

    @Test
    void registerUserWithIncorrectPassword_Test() throws Exception {
        ArrayList<UserDTO> userDTOList = getUsersList(
            true,
            false,
            true);
        for (UserDTO userDTO : userDTOList) {
            registerUserWithIncorrectPassword_returnValidationExceptionResponseBody(userDTO);
        }
    }

    @Test
    void registerUserWithIncorrectEmail_Test() throws Exception {
        ArrayList<UserDTO> userDTOList = getUsersList(
                true,
                true,
                false);
        for (UserDTO userDTO : userDTOList) {
            registerUserWithIncorrectEmail_returnValidationExceptionResponseBody(userDTO);
        }
    }

    @Test
    void registerUserWithIncorrectNameAndPassword_Test() throws Exception {
        ArrayList<UserDTO> userDTOList = getUsersList(
                false,
                false,
                true);
        for (UserDTO userDTO : userDTOList) {
            registerUserWithIncorrectNameAndPassword_returnValidationExceptionResponseBody(userDTO);
        }
    }

    @Test
    void registerUserWithIncorrectNameAndEmail_Test() throws Exception {
        ArrayList<UserDTO> userDTOList = getUsersList(
                false,
                true,
                false);
        for (UserDTO userDTO : userDTOList) {
            registerUserWithIncorrectNameAndEmail_returnValidationExceptionResponseBody(userDTO);
        }
    }

    @Test
    void registerUserWithIncorrectPasswordAndEmail_Test() throws Exception {
        ArrayList<UserDTO> userDTOList = getUsersList(
                true,
                false,
                false);
        for (UserDTO userDTO : userDTOList) {
            registerUserWithIncorrectPasswordAndEmail_returnValidationExceptionResponseBody(userDTO);
        }
    }

    @Test
    void registerUserWithIncorrectNameAndPasswordAndEmail_Test() throws Exception {
        ArrayList<UserDTO> userDTOList = getUsersList(
                false,
                false,
                false);
        for (UserDTO userDTO : userDTOList) {

            registerUserWithIncorrectNameAndPasswordAndEmail_returnValidationExceptionResponseBody(userDTO);
        }
    }


    void registerCorrectUser_returnSuccessRegistrationResponse(UserDTO userDTO) throws Exception {
        when(service.addUserToDB(any(UserDTO.class))).thenReturn(new SuccessRegistrationResponseBody());
        mockMvc.perform(post("/registration/v0/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDTO)))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.status").value("OK"))
                .andExpect(jsonPath("$.trace.message").value("Успешная регистрация"));
        verify(service,times(1)).addUserToDB(any(UserDTO.class));
    }

    void registerUserWithSameEmail_returnNotUniqueEmailResponse(UserDTO userDTO) throws Exception {
        when(service.addUserToDB(any(UserDTO.class))).thenReturn(new NotUniqueEmailResponseBody(new NotUniqueEmailException(userDTO)));
        mockMvc.perform(post("/registration/v0/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDTO)))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.trace.message").value(
                        String.format("Пользователь с таким адресом электронной почты (%s) уже существует!",
                                userDTO.getEmail())
                ))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"));

        verify(service,times(1)).addUserToDB(any(UserDTO.class));
    }

    void registerUserWithIncorrectName_returnValidationExceptionResponseBody(UserDTO userDTO) throws Exception {

        when(service.addUserToDB(any(UserDTO.class))).thenReturn(new SuccessRegistrationResponseBody());
        mockMvc.perform(post("/registration/v0/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDTO)))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.trace.message").isArray())
                .andExpect(jsonPath("$.trace.message[0].fieldName").value("username"));

        verifyNoInteractions(service);
    }

    void registerUserWithIncorrectPassword_returnValidationExceptionResponseBody(UserDTO userDTO) throws Exception {
        when(service.addUserToDB(any(UserDTO.class))).thenReturn(new SuccessRegistrationResponseBody());
        mockMvc.perform(post("/registration/v0/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDTO)))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.trace.message[0].fieldName").value("password"));

        verifyNoInteractions(service);
    }

    void registerUserWithIncorrectEmail_returnValidationExceptionResponseBody(UserDTO userDTO) throws Exception {
        when(service.addUserToDB(any(UserDTO.class))).thenReturn(new SuccessRegistrationResponseBody());
        mockMvc.perform(post("/registration/v0/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDTO)))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.trace.message[0].fieldName").value("email"));
        verifyNoInteractions(service);
    }

    void registerUserWithIncorrectNameAndPassword_returnValidationExceptionResponseBody(UserDTO userDTO) throws Exception {
        when(service.addUserToDB(any(UserDTO.class))).thenReturn(new SuccessRegistrationResponseBody());
        mockMvc.perform(post("/registration/v0/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDTO)))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.trace.message").isArray())
                .andExpect(jsonPath("$.trace.message[0].fieldName").value(Matchers.anyOf(
                        Matchers.hasToString("username"),
                        Matchers.hasToString("password"))))
                .andExpect(jsonPath("$.trace.message[1].fieldName").value(Matchers.anyOf(
                        Matchers.hasToString("username"),
                        Matchers.hasToString("password"))));
        verifyNoInteractions(service);
    }

    void registerUserWithIncorrectNameAndEmail_returnValidationExceptionResponseBody(UserDTO userDTO) throws Exception {
        when(service.addUserToDB(any(UserDTO.class))).thenReturn(new SuccessRegistrationResponseBody());

        mockMvc.perform(post("/registration/v0/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDTO)))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.trace.message").isArray())
                .andExpect(jsonPath("$.trace.message[0].fieldName").value(Matchers.anyOf(
                        Matchers.hasToString("email"),
                        Matchers.hasToString("username"))))
                .andExpect(jsonPath("$.trace.message[1].fieldName").value(Matchers.anyOf(
                        Matchers.hasToString("email"),
                        Matchers.hasToString("username"))));
        verifyNoInteractions(service);
    }

    void registerUserWithIncorrectPasswordAndEmail_returnValidationExceptionResponseBody(UserDTO userDTO) throws Exception {
        when(service.addUserToDB(any(UserDTO.class))).thenReturn(new SuccessRegistrationResponseBody());
        mockMvc.perform(post("/registration/v0/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDTO)))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.trace.message").isArray())
                .andExpect(jsonPath("$.trace.message[0].fieldName").value(Matchers.anyOf(
                        Matchers.hasToString("email"),
                        Matchers.hasToString("password"))))
                .andExpect(jsonPath("$.trace.message[1].fieldName").value(Matchers.anyOf(
                        Matchers.hasToString("email"),
                        Matchers.hasToString("password"))));
        verifyNoInteractions(service);
    }

    void registerUserWithIncorrectNameAndPasswordAndEmail_returnValidationExceptionResponseBody(UserDTO userDTO) throws Exception {
        when(service.addUserToDB(any(UserDTO.class))).thenReturn(new SuccessRegistrationResponseBody());
        mockMvc.perform(post("/registration/v0/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(userDTO)))
                .andExpect(status().is(400))
                .andExpect(jsonPath("$.trace.message").isArray())
                .andExpect(jsonPath("$.status").value("BAD_REQUEST"))
                .andExpect(jsonPath("$.trace.message[0].fieldName").value(Matchers.anyOf(
                        Matchers.hasToString("email"),
                        Matchers.hasToString("username"),
                        Matchers.hasToString("password"))))
                .andExpect(jsonPath("$.trace.message[1].fieldName").value(Matchers.anyOf(
                        Matchers.hasToString("email"),
                        Matchers.hasToString("username"),
                        Matchers.hasToString("password"))))
                .andExpect(jsonPath("$.trace.message[2].fieldName").value(Matchers.anyOf(
                        Matchers.hasToString("email"),
                        Matchers.hasToString("username"),
                        Matchers.hasToString("password"))))
        ;
        verifyNoInteractions(service);
    }



}