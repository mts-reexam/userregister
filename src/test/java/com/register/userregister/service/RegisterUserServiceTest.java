package com.register.userregister.service;


import com.register.userregister.dto.UserDTO;
import com.register.userregister.mapper.UserToEntityMapper;
import com.register.userregister.repository.RegisterRepository;
import com.register.userregister.response.abstraction.AbstractResponseBody;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class RegisterUserServiceTest {

    @Mock
    private RegisterRepository registerRepository;

    @Mock
    private UserToEntityMapper mapper;

    @InjectMocks
    private RegisterUserServiceImpl registerUserService;

    @Mock
    UserDTO userDTO;



    @Test
    void addUserToDBWithCorrectUser() {
        when(registerRepository.existsById(userDTO.getEmail())).thenReturn(false);
        AbstractResponseBody response = registerUserService.addUserToDB(userDTO);

        assertNotNull(response);
        verify(registerRepository).existsById(userDTO.getEmail());
        verify(mapper).mapUserToEntity(userDTO);
        verify(registerRepository).save(mapper.mapUserToEntity(userDTO));
    }

    @Test
    void addUserToDBWithIncorrectUser() {
        when(registerRepository.existsById(userDTO.getEmail())).thenReturn(true);

        registerUserService.addUserToDB(userDTO);
        verify(registerRepository).existsById(userDTO.getEmail());
        verifyNoInteractions(mapper);
        verifyNoMoreInteractions(registerRepository);
    }
}
