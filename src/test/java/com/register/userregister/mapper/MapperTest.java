package com.register.userregister.mapper;


import com.register.userregister.dto.UserDTO;
import com.register.userregister.entity.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.register.userregister.util.MockUser.mockUser;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(classes = {UserToEntityMapper.class})
public class MapperTest {

    @Autowired
    UserToEntityMapper mapper;


    @Test
    void mapUserToEntityTest() {
        UserDTO userDTO = mockUser("user","12345","user@gmail.com");
        UserEntity entity = mapper.mapUserToEntity(userDTO);

        assertNotNull(entity);
        assertEquals(entity.getEmail(), userDTO.getEmail());
        assertEquals(entity.getPassword(), userDTO.getPassword());
        assertEquals(entity.getUsername(), userDTO.getUsername());
        assertFalse(entity.isActivated());

    }



}
