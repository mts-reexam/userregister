package com.register.userregister.controller;

import com.register.userregister.dto.UserDTO;
import com.register.userregister.response.abstraction.AbstractResponseBody;
import com.register.userregister.service.RegisterUserService;
import com.register.userregister.service.RegisterUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/registration/v0")
public class RegistrationController {


    private final RegisterUserService registerService;

    @Autowired
    public RegistrationController(RegisterUserServiceImpl registerService){
        this.registerService = registerService;
    }

    @PostMapping(value = "/users")
    public ResponseEntity<AbstractResponseBody> registerUser(@Valid @RequestBody UserDTO userDTO) {
        AbstractResponseBody response = registerService.addUserToDB(userDTO);
        return new ResponseEntity<>(response, response.getStatus()) ;
    }


}
