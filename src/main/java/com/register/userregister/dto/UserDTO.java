package com.register.userregister.dto;


import lombok.Getter;
import lombok.NonNull;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.Locale;

@Getter
public class UserDTO {

    @NonNull
    @Size(min = 2,max = 20)
    private String username;

    @NonNull
    @Size(min = 5,max = 30)
    private char[] password;

    @NonNull
    @Email(regexp = "[a-zA-Z0-9]+[@]{1}+[a-zA-Z0-9]+[.]{1}+[a-zA-Z]+")
    @Size(max = 256)
    private String email;

    public UserDTO(String username, char[] password, String email){
        this.username = username;
        this.password = password;
        this.email = email.toLowerCase(Locale.ROOT);
    }


}
