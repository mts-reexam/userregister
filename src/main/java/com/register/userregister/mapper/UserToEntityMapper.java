package com.register.userregister.mapper;

import com.register.userregister.dto.UserDTO;
import com.register.userregister.entity.UserEntity;
import org.springframework.stereotype.Component;

@Component
public class UserToEntityMapper {

    public UserEntity mapUserToEntity(UserDTO userDTO){
        UserEntity userEntity = new UserEntity(
                userDTO.getUsername(),
                userDTO.getPassword(),
                userDTO.getEmail(),
                false
        );
        return userEntity;
    }

}
