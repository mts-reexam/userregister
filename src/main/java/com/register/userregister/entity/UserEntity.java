package com.register.userregister.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "registered_users")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity {

    @NotNull
    @Column(name = "username")
    private String username;

    @NotNull
    @Column(name = "password")
    private char[] password;

    @NonNull
    @Id
    @Column(name = "email")
    private String email;

    @Column(name = "activated")
    private boolean isActivated;

}
