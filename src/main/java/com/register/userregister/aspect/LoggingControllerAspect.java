package com.register.userregister.aspect;


import com.register.userregister.dto.UserDTO;
import com.register.userregister.response.abstraction.AbstractResponseBody;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Arrays;


@Aspect
@Component
public class LoggingControllerAspect {

    private final Logger log = LogManager.getLogger(this.getClass());

    @Pointcut("execution(* com.register.userregister.controller.RegistrationController.registerUser(*)) && args(userDTO)")
    public void registerUserAtControllerPointCut(UserDTO userDTO) {
    }


    @Around(value = "registerUserAtControllerPointCut(userDTO)", argNames = "pjp,userDTO")
    public Object aroundRegisterUserMethod(ProceedingJoinPoint pjp, UserDTO userDTO) throws Throwable {
        log.info(String.format("Пользователь {Имя: %s, почта: %s, пароль: %s} - попытка регистрации ",
                userDTO.getEmail(),
                userDTO.getUsername(),
                Arrays.toString(userDTO.getPassword())
        ));
        return pjp.proceed();
    }


    @AfterReturning(value = "registerUserAtControllerPointCut(userDTO)", returning = "response", argNames = "userDTO,response")
    public ResponseEntity<AbstractResponseBody> afterRegisterUserMethod(UserDTO userDTO, ResponseEntity<AbstractResponseBody> response) {
        log.info(String.format("Пользователь {Имя: %s, почта: %s, пароль: %s} - %s",
                userDTO.getEmail(),
                userDTO.getUsername(),
                Arrays.toString(userDTO.getPassword()),
                response.getStatusCode().value() == 200 ? "успешно зарегистрирован" : "был отклонён"
        ));
        return response;
    }


}
