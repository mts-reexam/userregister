package com.register.userregister.aspect;


import com.register.userregister.response.validation.ErrorMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.function.Supplier;


@Aspect
@Component
public class LoggingControllerExceptionHandlerAspect {

    private final Logger log = LogManager.getLogger(this.getClass());

    @Pointcut("execution(* com.register.userregister.exceptionhandler.ControllerExceptionHandler.*(*)) && args(exception)")
    public void handleExceptionPointCut(Exception exception){}



    @After(value = "handleExceptionPointCut(exception)", argNames = "exception")
    public void afterHandleException(Exception exception){
        if (exception instanceof MethodArgumentNotValidException notValidException) {
            log.warn(String.format("Ошибка регистрации: %s",
                    notValidException.getBindingResult().getFieldErrors().stream()
                            .map(error -> new ErrorMessage(error.getField(),error.getRejectedValue() ,error.getDefaultMessage()))
                            .collect((Supplier<ArrayList<String>>) ArrayList::new,
                                    (list,error) -> list.add(
                                            String.format("Поле: %s |Значение: %s |Сообщение: %s",
                                            error.getFieldName(),
                                            error.getRejectedValue(),
                                            error.getMessage())
                                    ),
                                    (list1,list2) -> list2.addAll(list1))
            ));
        } else {
            log.warn(String.format("Ошибка регистрации: %s",
                    exception.getMessage()
            ));
        }

    }

//    @AfterReturning(value = "registerUserAtControllerPointCut(user)",returning = "response", argNames = "user,response")
//    public ResponseEntity<AbstractResponseBody> afterRegisterUserMethod(User user, ResponseEntity<AbstractResponseBody> response) {
//        log.info(String.format("Пользователь {Имя: %s, почта: %s, пароль: %s} - %s",
//                user.getEmail(),
//                user.getUsername(),
//                Arrays.toString(user.getPassword()),
//                response.getStatusCode().value() == 200 ? "успешно зарегистрирован" : "был отклонён"
//        ));
//        return response;
//    }



}
