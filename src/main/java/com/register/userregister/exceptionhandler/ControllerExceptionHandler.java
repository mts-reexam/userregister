package com.register.userregister.exceptionhandler;

import com.register.userregister.exception.NotUniqueEmailException;
import com.register.userregister.response.NotUniqueEmailResponseBody;
import com.register.userregister.response.abstraction.AbstractResponseBody;
import com.register.userregister.response.validation.ValidationExceptionResponseBody;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class ControllerExceptionHandler {


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<AbstractResponseBody> handleException(MethodArgumentNotValidException exception) {
        AbstractResponseBody response = new ValidationExceptionResponseBody(exception);
        return new ResponseEntity<>(response, response.getStatus());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    private ResponseEntity<AbstractResponseBody> handleException(HttpMessageNotReadableException exception) {
        AbstractResponseBody response = new ValidationExceptionResponseBody(exception);
        return new ResponseEntity<>(response, response.getStatus());
    }

    @ExceptionHandler(NotUniqueEmailException.class)
    public ResponseEntity<AbstractResponseBody> handleException(NotUniqueEmailException exception) {
        AbstractResponseBody response = new NotUniqueEmailResponseBody(exception);
        return new ResponseEntity<>(response, response.getStatus());
    }


}
