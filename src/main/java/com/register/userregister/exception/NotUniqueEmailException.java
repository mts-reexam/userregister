package com.register.userregister.exception;

import com.register.userregister.dto.UserDTO;

public class NotUniqueEmailException extends Exception {


    public NotUniqueEmailException(UserDTO userDTO){
        super(String.format("Пользователь с таким адресом электронной почты (%s) уже существует!", userDTO.getEmail()));
    }
}
