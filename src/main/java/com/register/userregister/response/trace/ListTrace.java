package com.register.userregister.response.trace;

import com.register.userregister.response.abstraction.AbstractTrace;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class ListTrace extends AbstractTrace {

    protected List<?> message;

}
