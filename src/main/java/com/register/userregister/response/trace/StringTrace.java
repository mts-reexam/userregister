package com.register.userregister.response.trace;


import com.register.userregister.response.abstraction.AbstractTrace;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class StringTrace extends AbstractTrace {

    public String message;

}
