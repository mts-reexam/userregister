package com.register.userregister.response;

import com.register.userregister.response.abstraction.AbstractResponseBody;
import com.register.userregister.response.trace.StringTrace;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class SuccessRegistrationResponseBody extends AbstractResponseBody {

    public SuccessRegistrationResponseBody(){
        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.status = HttpStatus.OK;
        this.trace = new StringTrace("Успешная регистрация");
    }
}
