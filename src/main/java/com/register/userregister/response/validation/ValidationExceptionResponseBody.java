package com.register.userregister.response.validation;


import com.register.userregister.response.abstraction.AbstractResponseBody;
import com.register.userregister.response.trace.ListTrace;
import com.register.userregister.response.trace.StringTrace;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.stream.Collectors;

public class ValidationExceptionResponseBody extends AbstractResponseBody {

    public ValidationExceptionResponseBody(MethodArgumentNotValidException exception){
        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.status = HttpStatus.BAD_REQUEST;
        this.trace = new ListTrace(exception.getBindingResult().getFieldErrors().stream()
                .map(error -> new ErrorMessage(error.getField(),error.getRejectedValue(), error.getDefaultMessage()))
                .collect(Collectors.toList()));
    }

    public ValidationExceptionResponseBody(HttpMessageNotReadableException exception){

        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.status = HttpStatus.BAD_REQUEST;
        this.trace = new StringTrace( "Возможно одно из полей содержит null");

    }




}

