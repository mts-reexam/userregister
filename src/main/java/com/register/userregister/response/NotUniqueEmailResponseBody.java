package com.register.userregister.response;

import com.register.userregister.exception.NotUniqueEmailException;
import com.register.userregister.response.abstraction.AbstractResponseBody;
import com.register.userregister.response.trace.StringTrace;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class NotUniqueEmailResponseBody extends AbstractResponseBody {

    public NotUniqueEmailResponseBody(NotUniqueEmailException exception) {
        this.timestamp = Timestamp.valueOf(LocalDateTime.now());
        this.status = HttpStatus.BAD_REQUEST;
        this.trace = new StringTrace(exception.getMessage());
    }


}
