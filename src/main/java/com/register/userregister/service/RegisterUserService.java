package com.register.userregister.service;

import com.register.userregister.dto.UserDTO;
import com.register.userregister.response.abstraction.AbstractResponseBody;

public interface RegisterUserService {


    AbstractResponseBody addUserToDB(UserDTO userDTO);


}
