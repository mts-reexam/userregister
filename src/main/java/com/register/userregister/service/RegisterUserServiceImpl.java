package com.register.userregister.service;


import com.register.userregister.dto.UserDTO;
import com.register.userregister.exception.NotUniqueEmailException;
import com.register.userregister.mapper.UserToEntityMapper;
import com.register.userregister.repository.RegisterRepository;
import com.register.userregister.response.NotUniqueEmailResponseBody;
import com.register.userregister.response.SuccessRegistrationResponseBody;
import com.register.userregister.response.abstraction.AbstractResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegisterUserServiceImpl implements RegisterUserService{

    private final RegisterRepository registerRepository;

    private final UserToEntityMapper mapper;

    @Autowired
    public RegisterUserServiceImpl(RegisterRepository registerRepository, UserToEntityMapper userToEntityMapper){
        this.registerRepository = registerRepository;
        this.mapper = userToEntityMapper;
    }

    @Override
    public AbstractResponseBody addUserToDB(UserDTO userDTO) {
        try {
            if (registerRepository.existsById(userDTO.getEmail())) {
                throw new NotUniqueEmailException(userDTO);
            }
            registerRepository.save(mapper.mapUserToEntity(userDTO));
            return new SuccessRegistrationResponseBody();
        } catch (NotUniqueEmailException exception){
            return new NotUniqueEmailResponseBody(exception);
        }
    }


}
